// Ui.js
// =====
module.exports = {
	Ui
}

var uuid = require('node-uuid');

function Ui() {
	this.elements = {
		chatlog: document.querySelector('[name="chatlog"]'),
		chatContainer: document.getElementById('chatContainer'),
		inputForm: document.querySelector('[name="chat"]'),
		inputField: document.getElementById('chatinput'),
		playerName: document.querySelector('.playername'),
		playerLevel: document.querySelector('.player_level'),
		playerCoinCount: document.querySelector('.player_coinCount'),
		loginView: document.querySelector('[name="loginform"]'),
		screennameField: document.getElementById('screenname'),
		duplicateSessionError: document.getElementById('duplicateSessionError')
	};
	this.conn = undefined;
	
	this.readyForScroll = false;
	
	this.elements.loginView.onsubmit = this.submitUserName.bind(this);
	
	this.elements.inputForm.onsubmit = this.submitChatMessage.bind(this);
}

Ui.prototype.submitUserName = function(e) {
	e.preventDefault();
	var name = this.elements.screennameField.value;
	this.conn.createUsername(name);
};

Ui.prototype.setUserName = function(name) {
	this.elements.playerName.textContent = name;
};

Ui.prototype.nameUnavailable = function() {
	alert('The screen name you requested is unavailable.');
};

Ui.prototype.duplicateSessionError = function() {
	this.elements.loginView.className = 'hidden';
	this.elements.chatContainer.className = 'hidden';
	this.elements.duplicateSessionError.className = '';
};

Ui.prototype.startGame = function() {
	this.elements.loginView.className = 'hidden';
	this.elements.chatContainer.className = 'loadingAnimated';
	window.setTimeout(function(){this.readyForScroll = true}.bind(this), 2000);
};

Ui.prototype.submitChatMessage = function(e) {
	e.preventDefault();
	var msgText = this.elements.inputField.value;
	if(msgText.charAt(0) == '/') {
		this.conn.sendChatCommand(msgText.substr(1));
	}
	else {
		this.conn.sendChatMessage(msgText);
	}
	this.elements.inputField.value = '';
};

Ui.prototype.showChatMessage = function(senderName, msgText, hideName) {
	var msgId = uuid.v4();
	
	var sender_field = '';
	if(!hideName) {
		sender_field = '<span class="sender-name"></span>';
	}
	this.elements.chatlog.innerHTML += '<p class="chat-msg" data-msgid="'+msgId+'">'+sender_field+'<span class="msg-text"></span></p>';
	
	if(!hideName) {
		document.querySelector('[data-msgid="'+msgId+'"] .sender-name').textContent = senderName;
	}
	document.querySelector('[data-msgid="'+msgId+'"] .msg-text').textContent = msgText;
	
	if(this.readyForScroll)
		document.querySelector('[data-msgid="'+msgId+'"]').scrollIntoView(true);
};

Ui.prototype.updateStatus = function(status) {
	this.elements.playerCoinCount.textContent = status.gold;
	this.elements.playerLevel.textContent = status.level;
};