// Connection.js
// =============
module.exports = {
	Connection
}

function Connection(socket, gui) {
	this.sock = socket;
	
	this.gui = gui;
	
	this.connId = undefined;
	this.userKey = undefined;
	this.userName = undefined;
	
	this.loadUserKey();
	
	this.sock.onmessage = this.handleMsg.bind(this);
}

Connection.prototype.handleMsg = function(e) {
	var msg = JSON.parse(e.data);
	
	switch(msg.type) {
		case 'begin':
			this.connId = msg.connId;
			console.log('opened connection with connId: ' + this.connId);
			if(this.userKey) {
				this.sock.send(JSON.stringify({type: 'returninguser', connId: this.connId, userKey: this.userKey}));
			}
			break;
		
		case 'newuserloggedin':
			this.userName = msg.userName;
			this.userKey = msg.userKey;
			this.saveUserKey();
			console.log('created new user ' + this.userName);
			this.gui.setUserName(this.userName);
			this.gui.startGame();
			break;
			
		case 'namenotavailable':
			console.log('requested user name unavailable');
			this.gui.nameUnavailable();
			break;
		
		case 'loginsuccess':
			this.userName = msg.userName;
			console.log('logged in as user ' + this.userName);
			this.gui.setUserName(this.userName);
			this.gui.startGame();
			break;
		
		case 'loginfail':
			console.warn('server rejected user key, clearing key and reloading');
			this.logout();
			break;
		
		case 'duplicatesession':
			this.gui.duplicateSessionError();
			break;
		
		case 'displaymsg':
			console.info('server sent message to be displayed to the user: ', msg.contents);
			this.gui.showChatMessage(msg.contents.senderName, msg.contents.senderMsg, msg.contents.textOnly);
			break;
		
		case 'statusmsg':
			this.gui.updateStatus(msg.contents);
			break;
		
		default:
			console.warn('Unknown message type received over socket. Contents: ', msg);
	}
};

Connection.prototype.loadUserKey = function() {
	if(localStorage && localStorage.chattyUserKey) {
		this.userKey = localStorage.chattyUserKey;
	}
};

Connection.prototype.saveUserKey = function() {
	if(localStorage) {
		localStorage.chattyUserKey = this.userKey;
	}
	else {
		alert('Because you are using private browsing mode or your browser does not support HTML5 Local Storage, if you reload the page you will lose your username.');
	}
};

Connection.prototype.createUsername = function(name) {
	if(this.userKey === undefined && this.userName === undefined) {
		this.sock.send(JSON.stringify({type: 'newuser', connId: this.connId, userName: name}));
	}
	else {
		console.warn('could not attempt to create a new username because you appear to have one already');
	}
};

Connection.prototype.sendChatMessage = function(msgText) {
	this.sock.send(JSON.stringify({type: 'chatMsg', connId: this.connId, contents: {type: 'globalChat', userMsg: msgText}}));
};

Connection.prototype.sendChatCommand = function(command) {
	if(command == 'logout') {
		this.logout();
	}
	else {
		this.sock.send(JSON.stringify({type: 'chatMsg', connId: this.connId, contents: {type: 'command', userMsg: command}}));
	}
};

Connection.prototype.logout = function() {
	if(localStorage) {
		localStorage.removeItem('chattyUserKey');
	}
	window.location.reload(false);
}